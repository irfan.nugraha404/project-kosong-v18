import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { ProviderData } from "./stateManagement/Context";
import Home from "./pages/home";

function App() {
  return (
    <ProviderData>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
      </Router>
    </ProviderData>
  );
}

export default App;
