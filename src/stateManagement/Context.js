import { createContext, useState } from "react";

export const ContextData = createContext();

export const ProviderData = (props) => {
  const [dataAPI, setDataAPI] = useState([]);

  return (
    <ContextData.Provider
      value={{
        dataAPI,
        setDataAPI,
      }}
    >
      {props.children}
    </ContextData.Provider>
  );
};
